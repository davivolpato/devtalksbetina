//+------------------------------------------------------------------+
//|                                                     Exemplo3.mq5 |
//|                                                         Devtalks |
//|                                         https://www.devtalks.com |
//+------------------------------------------------------------------+
#property copyright "Devtalks"
#property link      "https://www.devtalks.com"
#property version   "1.00"

// Includes
#include <Trade\Trade.mqh>          // Classe para facilitar o acesso às funções de negociação.
#include <Trade\SymbolInfo.mqh>     // Classe para facilitar o acesso às propriedades do símbolo (ativo).

// Variáveis do Robô
MqlRates          preco[];
CTrade            operacao;
CSymbolInfo       ativo;
bool              compraEmAberto;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   Print("Robô Betina Iniciado");
   
   // Define o ID do robô para restrear ordens executadas
   operacao.SetExpertMagicNumber(112233445566);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---

   // VARIÁVEIS PARA IDENTIFICAR SE EXISTEM POSIÇÕES ABERTAS
   compraEmAberto=false;
   if(PositionSelect(_Symbol) == true && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY) 
   {
      compraEmAberto = true;  //Tem uma compra
   }

   // SÓ QUEREMOS QUE FAÇA ALGUMA COISA A CADA NOVA BARRA
   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0)
   {
      if(Old_Time!=New_Time[0])
      {
         IsNewBar=true;
         Old_Time=New_Time[0];
      }
   }
   else
   {
      Print("Erro ao copiar hitórico das barras ",GetLastError());
      ResetLastError();
      return;
   }
   
   if (IsNewBar) {
      Print("Nova barra aqui: ",New_Time[0]);
      pegarPrecoUltimaBarra();
      
      // Verificar confições para comprar
      executarCompra();
   }
   
  }
//+------------------------------------------------------------------+

void pegarPrecoUltimaBarra() {   
   // Indica que a array "preco" vai trabalhar os dados em ordem reversa
   ArraySetAsSeries(preco,true); 
   
   // Copia os preços das barras para o array "preco"
   CopyRates(Symbol(),Period(),0,Bars(Symbol(),Period()),preco);
   
   // Recupera as informações da última barra fechada
   double abertura = preco[1].open;
   double fechamento = preco[1].close;
   double maximo = preco[1].high;
   double minimo = preco[1].low;
   
   Print("==== DADOS DA ÚLTIMA BARRA ====");
   Print("Preço de abertura: " + abertura);
   Print("Preço de fechamento: " + fechamento);
   Print("Preço máximo: " + maximo);
   Print("Preço mínimo: " + minimo);
}

void executarCompra() {
   double diferenca = preco[1].close - preco[1].open;
   
   if ( !compraEmAberto && diferenca >= 50 ) {
      
      // Atualiza os preços atuais do ativo
      ativo.RefreshRates();
      
     // Descobre o melhor preço de venda atual
      double melhorPrecoVenda = NormalizeDouble(ativo.Bid(),0);
      
      // Preço de Ganho
      double gain = NormalizeDouble(melhorPrecoVenda+100,0);
      
      // Preço de Perda
      double loss = NormalizeDouble(melhorPrecoVenda-150,0);
      
     // Realiza a compra
     operacao.Buy(5,_Symbol,melhorPrecoVenda,loss,gain,"Ordem de compra pela Betina");
   }
   
}