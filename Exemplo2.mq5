//+------------------------------------------------------------------+
//|                                                     Exemplo2.mq5 |
//|                                                         Devtalks |
//|                                         https://www.devtalks.com |
//+------------------------------------------------------------------+
#property copyright "Devtalks"
#property link      "https://www.devtalks.com"
#property version   "1.00"


// Variáveis do Robô
MqlRates          preco[]; // * MqlRates: É uma estrutura de dados que armazena informações sobre os preços, volumes e spread.


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   Print("Robô Betina Iniciado");
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   // SÓ QUEREMOS QUE FAÇA ALGUMA COISA A CADA NOVA BARRA
   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0)
   {
      if(Old_Time!=New_Time[0])
      {
         IsNewBar=true;
         Old_Time=New_Time[0];
      }
   }
   else
   {
      Print("Erro ao copiar hitórico das barras ",GetLastError());
      ResetLastError();
      return;
   }
   
   if (IsNewBar) {
      Print("Nova barra aqui: ",New_Time[0]);
      pegarPrecoUltimaBarra();
   }
   
  }
//+------------------------------------------------------------------+

void pegarPrecoUltimaBarra() {   
   // Indica que a array "preco" vai trabalhar os dados em ordem reversa
   ArraySetAsSeries(preco,true); 
   
   // Copia os preços das barras para o array "preco"
   CopyRates(Symbol(),Period(),0,Bars(Symbol(),Period()),preco);
   
   // Recupera as informações da última barra fechada
   double abertura = preco[1].open;
   double fechamento = preco[1].close;
   double maximo = preco[1].high;
   double minimo = preco[1].low;
   
   Print("==== DADOS DA ÚLTIMA BARRA ====");
   Print("Preço de abertura: " + abertura);
   Print("Preço de fechamento: " + fechamento);
   Print("Preço máximo: " + maximo);
   Print("Preço mínimo: " + minimo);
}