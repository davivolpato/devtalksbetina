//+------------------------------------------------------------------+
//|                                                     Exemplo1.mq5 |
//|                                                         Devtalks |
//|                                         https://www.devtalks.com |
//+------------------------------------------------------------------+
#property copyright "Devtalks"
#property link      "https://www.devtalks.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   Print("Robô Betina Iniciado");
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   // SÓ QUEREMOS QUE FAÇA ALGUMA COISA A CADA NOVA BARRA
   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0)
   {
      if(Old_Time!=New_Time[0])
      {
         IsNewBar=true;
         Old_Time=New_Time[0];
      }
   }
   else
   {
      Print("Erro ao copiar hitórico das barras ",GetLastError());
      ResetLastError();
      return;
   }
   
   
   
   if (IsNewBar) {
      Print("Nova barra aqui: ",New_Time[0]);
   }
   
  }
//+------------------------------------------------------------------+
